package com.xyz.android_home_6_2;

import java.util.ArrayList;

/**
 * Created by user on 22.01.2018.
 */


public class Generator {

    public Generator() {
    }

    public static ArrayList<Product> generate() {

        int itemCount = 5;

        ArrayList<Product> products = new ArrayList<Product>(itemCount);

        for (int i = 0; i < itemCount; i++) {

            products.add(new Product("Name " + i, i * 20 + 30 - i));
        }

        return products;


    }

}
