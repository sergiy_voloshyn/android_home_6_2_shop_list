package com.xyz.android_home_6_2;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
2. Создать программу "Товарооборот". На экране есть список товаров (5 наименований, больше не берите)
 и их кол-во. Каждое n количество времени в магазине покупают несколько товаров. Список каждый раз
 должен обновляться.
//in activity
listView.notifyDataSetChanged();// метод, который перегружает адаптер, если список данных изменился
2.1* Реализовать остановку работы магазина и возобновление. Покупатели, пришедшие во время перерыва
- игнорируются.
2.2** Реализовать остановку работы магазина и возобновление с созданием очереди покупателей
(за время перерыва покупатели создают очередь запросов, а при возобновлении работы все запросы
 покупателей, возникшие за время перерыва, должны быть удовлетворены)
 */
public class MainActivity extends AppCompatActivity {
    @BindView(R.id.list)
    RecyclerView list;

    ProductRecyclerAdapter adapter;
    ArrayList<Product> productArrayList = Generator.generate();

    public static final int START = 1;
    public static final int STOP = 2;
    int time = 1000;
    boolean isStarted;

    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                saleProduct();
                adapter.notifyDataSetChanged();
                h.sendEmptyMessageDelayed(START, time);
            }
            if (msg.what == STOP) {
                stopTimer();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        adapter = new ProductRecyclerAdapter(productArrayList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

    }

    @OnClick(R.id.button_buy)
    public void onClick() {
        if (isStarted) {
            stopTimer();
        } else {
            startTimer();
        }
    }

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);

    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }


    public void saleProduct() {

        final Random random = new Random();
        for (int i = 0; i < productArrayList.size(); i++) {

            Product product = new Product();
            product.setName( productArrayList.get(i).getName());
            product.setCount( productArrayList.get(i).getCount() - random.nextInt(10));

            if (product.getCount() < 0) product.setCount(0);

            productArrayList.set(i, product);

        }

    }

}
