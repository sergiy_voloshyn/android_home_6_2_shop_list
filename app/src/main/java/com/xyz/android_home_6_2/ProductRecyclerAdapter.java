package com.xyz.android_home_6_2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 22.01.2018.
 */

public class ProductRecyclerAdapter extends RecyclerView.Adapter
        <ProductRecyclerAdapter.ViewHolder> {

    private ArrayList<Product> list;
    private Context context;

    public ProductRecyclerAdapter(ArrayList<Product> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateData(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        Product product;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.count)
        EditText count;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void updateData(Product product) {
            this.product = product;
            name.setText(product.getName());
            count.setText(product.getCount().toString());
        }

    }


}
